#!/usr/bin/env python
#
#Author: Matt Soper (Truepath Technologies, Inc.)
#
# -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
# Copyright 2017 TruePath Technologies Inc
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
# Description: This plugin will let you get cpu and memory usage for processes you are monitoring with monit
#
# Nagios/OP5 check_monit_tpt.py plugin
#
VERSION="1.8"

import json, ast
import xmltodict
from optparse import OptionParser
import requests
import sys
import re

options = None

ok = []
warning = []
critical = []
svc_types = {}
svc_monitored = []
perfdata_string = ""

# This will get pull the monit services in XML format
def get_xml():
    requests.packages.urllib3.disable_warnings()
    response = requests.get('http://' + options.host + ':' + str(options.port) + '/_status?format=xml', auth=(options.username, options.password))
    all_stats = ast.literal_eval(json.dumps(xmltodict.parse(response.text)['monit']['service']))
    return all_stats

# This will loop through all services to then create dict with the number due to must pass index not str for service name
def process_svc_types(all_stats):
    counter = -1
    for item in all_stats:
         counter += 1
         svc_types.update({item['name']: counter})
    return svc_types

# This will process service status
def process_system_status(service):
    status = int(service['status'])
    if status != 0:
        critical.append("Critical: Service not running")
    else:
        ok.append('OK: Service is running: %s'%(service))

# This is to process service CPU
def process_system_cpu(service):
    if options.service == 'system_localhost':
        cpu_user = service['system']['cpu']['user']
        cpu_system = service['system']['cpu']['system']
        cpu_wait = service['system']['cpu']['wait']
        if float(cpu_user) >= float(options.critical):
            critical.append('Critical - Memory Percent: %s Memory KB: %s | cpu_user=%s%s;%s;%s'%(cpu_user,cpu_system,cpu_wait,cpu_user,"%",options.warning,options.critical))
        elif float(cpu_user) >= float(options.warning):
            warning.append('Warning - CPU User:%s CPU System: %s CPU Wait: %s | cpu_user=%s%s;%s;%s'%(cpu_user,cpu_system,cpu_wait,cpu_user,"%",options.warning,options.critical))
        else:
            ok.append('OK - CPU User:%s CPU System: %s CPU Wait: %s | cpu_user=%s%s;%s;%s'%(cpu_user,cpu_system,cpu_wait,cpu_user,"%",options.warning,options.critical))
    else:
        cpu_pct = service['cpu']['percent']
        cpu_pct_total = service['cpu']['percenttotal']
        if float(cpu_pct_total) >= float(options.critical):
            critical.append('Critical: CPU Percent: %s CPU Percent Total: %s | cpu_pct_total=%s%s;%s;%s'%(cpu_pct,cpu_pct_total,cpu_pct_total,"%",options.warning,options.critical))
        elif float(cpu_pct_total) >= float(options.warning):
            warning.append('Warning: CPU Percent: %s CPU Percent Total: %s | cpu_pct_total=%s%s;%s;%s'%(cpu_pct,cpu_pct_total,cpu_pct_total,"%",options.warning,options.critical))
        else:
            ok.append('OK: CPU Percent: %s CPU Percent Total: %s | cpu_pct_total=%s%s;%s;%s'%(cpu_pct,cpu_pct_total,cpu_pct_total,"%",options.warning,options.critical))

# This is to process service Memory
def process_system_mem(service):
    if options.service == 'system_localhost':
        mem_kb = service['system']['memory']['kilobyte']
        mem_pct = service['system']['memory']['percent']
        if float(mem_pct) >= float(options.critical):
            critical.append('Critical - Memory Percent: %s Memory KB: %s | mem_pct=%s%s;%s;%s'%(mem_pct,mem_kb,mem_pct,"%",options.warning,options.critical))
        elif float(mem_pct) >= float(options.warning):
            warning.append('Memory Percent: %s Memory KB: %s | mem_pct=%s%s;%s;%s'%(mem_pct,mem_kb,mem_pct,"%",options.warning,options.critical))
        else:
            ok.append('OK - Memory Percent: %s Memory KB: %s | mem_pct=%s%s;%s;%s'%(mem_pct,mem_kb,mem_pct,"%",options.warning,options.critical))
    else:
        mem_pct = service['memory']['percent']
        mem_pct_total = service['memory']['percenttotal']
        mem_kb = service['memory']['kilobyte']
        mem_kb_total = service['memory']['kilobytetotal']
        if float(mem_pct_total) >= float(options.critical):
            critical.append('Critical: Memory Percent: %s Memory Percent Total: %s Memory KB: %s Memory KB Total:%s | mem_pct_total=%s%s;%s;%s mem_kb=%sKB;;;0;%s'%(mem_pct,mem_pct_total,mem_kb,mem_kb_total,mem_kb_total,mem_pct_total,"%",options.warning,options.critical,mem_kb,mem_kb_total))
        elif float(mem_pct_total) >= float(options.warning):
            warning.append('Warning: Memory Percent: %s Memory Percent Total: %s Memory KB: %s Memory KB Total:%s | mem_pct_total=%s%s;%s;%s mem_kb=%sKB;;;0;%s'%(mem_pct,mem_pct_total,mem_kb,mem_kb_total,mem_kb_total,mem_pct_total,"%",options.warning,options.critical,mem_kb,mem_kb_total))
        else:
            ok.append('OK - Memory Percent: %s Memory Percent Total: %s Memory KB: %s Memory KB Total: %s | mem_pct_total=%s%s;%s;%s mem_kb=%sKB;;;0;%s'%(mem_pct,mem_pct_total,mem_kb,mem_kb_total,mem_pct_total,"%",options.warning,options.critical,mem_kb,mem_kb_total))

#This will process the service
def process_service(service):
    if options.process_cpu:
        process_system_cpu(service)
    elif options.process_mem:
        process_system_mem(service)
    elif options.process_status:
        process_system_status(service)
    else:
        critical.append('Error cannot process please select option -C <cpu> or -M <mem> or -S <status>  for service: %s'%(options.service))

#This will process the XML response from Monit
def process_xml(all_stats):
    if options.service:
        process_svc_types(all_stats)
        process_service(all_stats[svc_types[options.service]])

def main():
    global options, perfdata_string, all_stats, svc_monitored
    p = OptionParser(usage="Usage: check_monit_tpt.py -H <host> -u <user> -P <password> -S <service> <-C or -M or -S> -w <warning> -c <critical>")
    p.add_option("-H","--host", dest="host", help="Hostname or IP address")
    p.add_option("-p","--port", dest="port", type="int", default=2812, help="Port (Default: %default)")
    p.add_option("-u","--username", dest="username", help="Username")
    p.add_option("-P","--password", dest="password", help="Password")
    p.add_option("-w","--warning", dest="warning", help="Integer for service to warn if at or above threshold")
    p.add_option("-c","--critical", dest="critical", help="Integer for service to alert critical if at or above threshold")
    p.add_option("-s","--service", dest="service", help="Select service you would like performance data on")
    p.add_option("-d","--debug", dest="debug", action="store_true", default=False, help="Print all debugging info")
    p.add_option("-v","--verbose", dest="verbose", action="store_true", default=False, help="Verbose plugin response")
    p.add_option("-M","--memory", dest="process_mem", action="store_true", default=False, help="Display memory performance data")
    p.add_option("-C","--cpu", dest="process_cpu", action="store_true", default=False, help="Display cpu performance data")
    p.add_option("-S","--status", dest="process_status", action="store_true", default=False, help="Display All stats for the specified service")
    p.add_option("-V","--version", dest="version", action="store_true", help="This option show the current version number of the program and exit")
    (options, args) = p.parse_args()

    if not options.host:
        p.error("No <host> defined!")
        sys.exit(1)
    
    if not options.username:
        p.error("No <username> defined!")
        sys.exit(1)
    
    if not options.password:
        p.error("No <password> defined!")
        sys.exit(1)

    if options.process_cpu or options.process_mem:
        if not options.warning:
            p.error("No <warning> defined!")
            sys.exit(1)
        elif not options.critical:
            p.error("No <critical> defined!")
            sys.exit(1)

    process_xml(get_xml())
    if not options.service:
        process_svc_types(get_xml())
        perfdata_string = ' '.join(svc_types)
        print "These are the services being monitor: " + perfdata_string
        sys.exit()

    if critical:
        perfdata_string = ' '.join(critical)
        print perfdata_string
        sys.exit(2)
    elif warning:
        perfdata_string = ' '.join(warning)
        print perfdata_string
        sys.exit(1)
    elif ok:
        perfdata_string = ' '.join(ok)
        print perfdata_string
        sys.exit(0)
    else:
        print "Unknown"
        sys.exit(3)

if __name__ == '__main__':
    main()