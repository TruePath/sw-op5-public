#!/bin/bash
# ------------------------------------------------------------------
# Copywrite:  TruePath Technologies Inc.
# License:    Apache 2.0
# Author:     Patrick Byrne
# Title:      check_rsvp
# Description:
#     A Nagios type plugin to monitor the resource reservation protocol (RSVP)
# ------------------------------------------------------------------
#    Todo:
# ------------------------------------------------------------------

version=1.0.0
snmpflags="-OvQU"

# --- Functions ----------------------------------------------------

version()	
{
  printf "Version: %s\\n" $version
}

usage() 
{
    cat <<"STOP"
    Usage: 

    check_rsvp.sh -c <string> -H <host>

    OPTIONS

      -h | --help     Print this message
      --version       Print the check version
      -c <string>     SNMP community string
      -H <host>       Hostname or IP
      -t <juniper>    Device type


STOP
}

print_plugin_out()
{
  # Check if performance data exists
  if [[ -z "$3" ]]; then
    printf "%s - %s\\n" "$1" "$2"
  else
    # Start building the performance data string
    perf_string=""
    # All remaining arguments should be formatted like: label=value
    for perf_item in "${@:3}"; do
      perf_string+="${perf_item};0;0;0;1; "
    done
    printf "%s - %s|%s\\n" "$1" "$2" "$perf_string"
  fi
}


# --- Options processing -------------------------------------------

while [[ $# -gt 0 ]]; do
  param=$1
  value=$2
  case $param in
    -h | --help | help)
      version
      usage
      exit
      ;;
    --version)
      version
      exit
      ;;
    -c)
      snmpcommunity="$value"
      shift
      ;;
    -H)
      snmphost="$value"
      shift
      ;;
    -t)
      devicetype="$value"
      shift
      ;;
    *)
      echo "Unknown argument: $param"
      usage
      exit
      ;;
  esac
  shift
done

# Check that a host is set
if [[ -z "$snmphost" ]]; then
  echo "Error: No host provided"
  usage
  exit 2
fi

# Set default device type
if [[ -z "$devicetype" ]]; then
  devicetype="juniper"
fi

# Set default SNMP community string
if [[ -z "$snmpcommunity" ]]; then
  snmpcommunity="public"
fi

# --- Body ---------------------------------------------------------

# Set the correct OID for the device type
case "$devicetype" in
  juniper)
    statusoid=".1.3.6.1.4.1.2636.3.30.1.1.1.3"
    ;;
  *)
    echo "Unknown device type: $devicetype"
    usage
    exit
    ;;
esac

# Poll the status OIDs and use mapfile to split the lines into an array
mapfile -t result <<< \
 "$(snmpwalk -v 2c -c "$snmpcommunity" $snmpflags "$snmphost" "$statusoid")"

# Loop over the results array
for line in "${result[@]}"; do
  # Check for "DOWN" status and return critical if true
  if [[ "$line" == "0" ]]; then
    plugin_status="CRIT"
    print_plugin_out "$plugin_status" "Session down"
    exit 2
  fi
done

# If no problems were found in the results array, return OK
plugin_status="OK"
print_plugin_out "$plugin_status" "${#result[@]} sessions active"
exit 0

