#!/bin/bash
# ------------------------------------------------------------------
# Copywrite:  TruePath Technologies Inc.
# License:    Apache 2.0
# Author:     Patrick Byrne / Douglas Mauro
# Title:      check_snmpmath
# Description:
#       Do mathematical operations on SNMP results 
# ------------------------------------------------------------------
#    Todo:
# ------------------------------------------------------------------

version=1.6.0
snmpgetflags="-OvQU"
perfdata_enabled="True"
thold_enabled="True"
oid_list=()
calced_val=0
exit_status=99
metric_label=""
perf_min=""
perf_max=""

# --- Functions ----------------------------------------------------

version()	
{
  printf "Version: %s\\n" $version
}

usage() 
{
    cat <<"STOP"
    Usage: 

    check_snmpmath.sh [OPTIONS] -ou <OID> -ot <OID> 
    check_snmpmath.sh [OPTIONS] -ou <OID> -d <Integer>

    OPTIONS

      -h            Print this message
      -v  <1,2c,3>  SNMP version
      -c  <string>  SNMP community string
      -H  <host>    SNMP target host
      -W  <1-100>   Warn at x|%x (default 70)
      -C  <1-100>   Critical at x|%x (default 90)
      -DP           Disable performance data output 
      -DT           Disable threshold checking 
      -d  <INT>     Divide OID_1 by this number
      -a  <OIDs>    A comma seperated list of OIDs to add together
      -l  <label>   Metric label
      -u  <unit>    Metric unit [s,us,ms,%,B,KB,MB,TB,c]
      -ou <OID1>    OID_1 - Amount Used OR OID used when dividing by -d #
      -ot <OID2>    OID_2 - Amount Total

      --help same as -h
      --version Print version

STOP
}

# Validate any number of arguments and return 0 if they're all numeric
validate_numeric()
{
  for number in "$@"; do
    num_re='^[0-9]+$'
    if ! [[ "$number" =~ $num_re ]]; then
      return 1
    fi
  done
  return 0
}

# Check the warning and critical thresholds using the existing global variables
check_thresholds()
{
  # Set OK status when threshold checking is disabled 
  if [[ "$thold_enabled" == "False" ]]; then
    plugin_status="OK"
    exit_status=0
    warn_trigger_value=""
    crit_trigger_value=""
    return
  fi

  # Check percent used status
  if [[ $(echo "$calced_val < $warn_trigger_value" | bc -l) = 1 ]]; then
    plugin_status="OK"
    exit_status=0
  elif [[ $(echo "$calced_val < $crit_trigger_value" | bc -l) = 1 ]]; then
    plugin_status="WARN"
    exit_status=1
  else
    plugin_status="CRIT"
    exit_status=2
  fi
}

# Format and print the plugin output per Nagios API spec
print_plugin_out()
{
  # Print short form if performance data is disabled
  if [[ "$perfdata_enabled" == "False" ]]; then
    printf "%s - %s%s \\n" "$plugin_status" "$calced_val" "$metric_unit"
  else
    printf "%s - %s%s |'%s'=%s%s;%s;%s;%s;%s\\n" \
      "$plugin_status" "$calced_val" "$metric_unit" "$metric_label" \
        "$calced_val" "$metric_unit" "$warn_trigger_value" \
        "$crit_trigger_value" "$perf_min" "$perf_max"
  fi
}


# --- Options processing -------------------------------------------

while [[ $# -gt 0 ]]; do
  param=$1
  value=$2
  case $param in
    -h | --help | help)
      usage
      exit
      ;;
    --version | version)
      version
      exit
      ;;
    -v)
      snmpversion="$value"
      shift
      ;;
    -c)
      snmpcommunity="$value"
      shift
      ;;
    -H)
      snmphost="$value"
      shift
      ;;
    -W)
      warn_trigger_value="$value"
      shift
      ;;
    -C)
      crit_trigger_value="$value"
      shift
      ;;
    -DP)
      perfdata_enabled="False"
      ;;
    -DT)
      thold_enabled="False"
      ;;
    -d)
      mode="divide"
      div_by="$value"
      shift
      ;;
    -a)
      mode="add"
      #Split the string into the OID list array
      IFS=',' read -r -a oid_list <<< "$value"
      shift
      ;;
    -l)
      metric_label="$value"
      shift
      ;;
    -u)
      metric_unit="$value"
      shift
      ;;
    -ou)
      oid_list[0]="$value"
      shift
      ;;
    -ot)
      oid_list[1]="$value"
      metric_label="%"
      metric_unit="%"
      perf_min="0"
      perf_max="100"
      shift
      ;;
    *)
      echo "Error: unknown parameter \"$param\""
      usage
      exit 3
      ;;
  esac
  shift
done

# --- Body ---------------------------------------------------------

# Check and set default the warning percent
[[ -z $warn_trigger_value ]] && warn_trigger_value=70
if ! validate_numeric "$warn_trigger_value"; then
  echo "Error: Invalid warning threshold"
  exit 3
fi

# Check and set default the critical percent
[[ -z $crit_trigger_value ]] && crit_trigger_value=90
if ! validate_numeric "$crit_trigger_value"; then
  echo "Error: Invalid critical threshold"
  exit 3
fi

# Use snmpget to pull both OIDs and mapfile to split the lines into an array
mapfile -t result <<< \
 "$(snmpget -v "$snmpversion" -c "$snmpcommunity" $snmpgetflags "$snmphost" "${oid_list[@]}")"
  
# Verify that we get numeric results
if ! validate_numeric "${result[@]}"; then
  echo "Error: Unexpected result from host"
  exit 3
fi

# Check the mode and proceed appropriately
if [[ "$mode" = "divide" ]]; then
  # Make sure Used OID and divide by are set
  if [[ -z ${oid_list[0]} || -z $div_by ]]; then
    echo "Error: Used OID and/or divide by, not set"
    usage
    exit 3
  fi
	
  # Verify that we get a number to divide by
  if ! validate_numeric "$div_by"; then
    echo "Error: Unexpected result from host or divide number invalid"
    exit 3
  fi

  # Find the percent used and round it properly
  calced_val="$(awk "BEGIN {printf \"%.2f\\n\", (${result[0]}/${div_by})}")"

  # Process output
  check_thresholds
  print_plugin_out
  exit "$exit_status"

elif [[ "$mode" = "add" ]]; then
  # Loop through the results and add them together
  for result_value in "${result[@]}"; do
    calced_val="$(echo "$calced_val + $result_value" | bc -l)"
  done

  # Process output
  check_thresholds
  print_plugin_out
  exit "$exit_status"

else # Percentage calculation
  # Make sure Used and Total OIDs are set 
  if [[ -z ${oid_list[0]} || -z ${oid_list[1]} ]]; then
    echo "Error: OIDs are not set"
    usage
    exit 1
  fi

  # Find the percent used and round it properly
  calced_val="$(awk "BEGIN {printf \"%.2f\\n\", (${result[0]}/${result[1]})*100}")"

  # Process output
  check_thresholds
  print_plugin_out
  exit "$exit_status"
fi
