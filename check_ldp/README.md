![Bash](https://img.shields.io/badge/bash-v4-blue.svg)
![Dependencies](https://img.shields.io/badge/dependencies-none-brightgreen.svg)
![Contributions welcome](https://img.shields.io/badge/contributions-welcome-orange.svg)

----

![](images/truepath.png)

## Basic Overview

This script provides a simple method to check the status of LDP sessions via SNMP.
It currently supports Juniper and Arista devices but may be extensible to other systems as well.

## CLI Usage

To simply test the script on your system before you install it.

```bash
./check_ldp.sh -c test -H x.x.x.x
OK - 6 sessions active
```

## Installation

Copy the provided script into a directory accessible by your monitoring server and create a check command to utilize it.
Next, add the service to a host and pass any required arguments (SNMP community string and device type in the below example). 

#### Check Command Example

```
define command{
	command_name check_ldp
	command_line $USER1$/custom/check_ldp.sh -H $HOSTADDRESS$ -c $ARG1$ -t $ARG2$
}
```

#### Service Example

```
define service{
	use                 local-service
	host_name           testclient
	service_description Check LDP Protocol Connections
	check_command       check_ldp!public!juniper
}
```

