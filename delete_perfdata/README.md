# tpt-delete-perfdata (tpt_delete_rrd.sh)

Create a list of all current RRD files and delete them in chunks of one hundred, seleeping for five minutes between each chunk to allow time to rebuild gracefully. 

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

Bash

### Installing

Clone the repository to the target workstation

```
git clone https://username@gitlab.com/TruePath/tpt-delete-perfdata.git
```

### Usage

```
tpt_delete_rrd.sh
```

## Authors

* **Matt Soper** - [msoper-tpt](https://gitlab.com/msoper-tpt)

## Contributors

* **Patrick Byrne** - [pbyrne-tpt](https://gitlab.com/pbyrne-tpt)
