#!/bin/bash
# -----------------------------------------------------------------------------
# Author: Matt Soper (TruePath Technologies, Inc.)
# Title: tpt-delete-rrd (tpt_delete_rrd.sh)
# Description:
#       A basic script to create flat file of files in perfdata
#       directory then will remove the file in chunks of 100, then sleeps
#       for 5 minutes before deleting another chunk of 100
# ------------------------------------------------------------------------------
#
FILE='/tmp/perfdata_rrd.txt' # Perfdata text file that is created
DEL_RRD_LOG='/tmp/deleted_rrd.log' # Deleted rrd log file
count=0
#
#--------------------------------------------------------------------------------
# This will check if perfdata text file already exists and create if it doesn't
#
if [ ! -f $FILE ]; then
  find /opt/monitor/op5/pnp/perfdata/ -iname "*.rrd" > $FILE
fi
#---------------------------------------------------------------------------------
#
# Loop that will run through the perfdata_rrd.txt file and remove rrd files from
# perfdata
while  [[ -s $FILE ]]
do
  awk 'NR==1' $FILE > $DEL_RRD_LOG
  sed -i '1d' $FILE
  cat $DEL_RRD_LOG | xargs rm -v
  count=$[count+1]
  if [ $count -eq 100 ]; then
    echo "Sleeping 5 minutes"
    sleep 5m
    count=$[count=0]
  fi
done
rm -f $FILE
echo "Completed"
